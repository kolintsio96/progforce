/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

var registerForm = document.querySelector('.registration__form'),
    registerFiled = registerForm.querySelectorAll('input'),
    phoneField = registerForm.querySelector('[name="phone"]'),
    userList = document.querySelector('.user-list'),
    tbody = userList.querySelector('.user-list__tbody'),
    winnerForm = document.querySelector('#winnerForm');

registerForm.addEventListener('submit', function (e) {
    e.preventDefault();
    winnerForm.querySelector('input').value = '';

    var data = getData(registerFiled),
        td = '<tr><td><input type="text" name="name" value="' + data.name + '" disabled></td><td><input type="text" name="last_name" value="' + data.last_name + '" disabled></td><td><input type="email" name="email" value="' + data.email + '" disabled></td><td><input type="tel" name="phone" value="' + data.phone + '" disabled></td><td><input type="date" name="date_birthday" value="' + data.date_birthday + '" disabled></td></tr>';

    tbody.innerHTML = tbody.innerHTML + td;

    clearField(registerFiled);
    editData();
});

var clearField = function clearField(fields) {
    fields.forEach(function (el, i) {
        el.value = '';
    });
};

var getData = function getData(elements) {
    var data = {};
    elements.forEach(function (el, i) {
        data[el.getAttribute('name')] = el.value;
    });
    return data;
};

var editData = function editData() {
    var tr = tbody.querySelectorAll('.user-list__tbody tr');
    tr.forEach(function (el, i) {
        el.addEventListener('click', function () {
            el.remove();
            window.scrollTo(0, 0);
            var fields = el.querySelectorAll('input'),
                data = getData(fields);

            registerFiled.forEach(function (field, index) {
                field.value = data[field.getAttribute('name')];
            });
        });
    });
};

var random = function random(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
};

var phoneMask = function phoneMask(el) {
    el.addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });
};

phoneMask(phoneField);

winnerForm.addEventListener('submit', function (e) {
    e.preventDefault();
    var tr = tbody.querySelectorAll('tr'),
        field = winnerForm.querySelector('input');
    if (tr.length == 0) {
        field.value = 'No registers users';
    } else {
        var trRandom = tr[random(1, tr.length) - 1],
            name = trRandom.querySelector('[name = "name"]').value,
            lastName = trRandom.querySelector('[name = "last_name"]').value,
            fullName = name + ' ' + lastName;
        field.value = fullName;
    }
});

/***/ })

/******/ });
//# sourceMappingURL=base.bundle.js.map