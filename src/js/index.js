let registerForm = document.querySelector('.registration__form'),
    registerFiled = registerForm.querySelectorAll('input'),
    phoneField = registerForm.querySelector('[name="phone"]'),
    userList = document.querySelector('.user-list'),
    tbody = userList.querySelector('.user-list__tbody'),
    winnerForm = document.querySelector('#winnerForm');

registerForm.addEventListener('submit', (e) => {
    e.preventDefault();
    winnerForm.querySelector('input').value = '';

    let data = getData(registerFiled),
        td = `<tr><td><input type="text" name="name" value="${data.name}" disabled></td><td><input type="text" name="last_name" value="${data.last_name}" disabled></td><td><input type="email" name="email" value="${data.email}" disabled></td><td><input type="tel" name="phone" value="${data.phone}" disabled></td><td><input type="date" name="date_birthday" value="${data.date_birthday}" disabled></td></tr>`;

    tbody.innerHTML = tbody.innerHTML + td;

    clearField(registerFiled);
    editData();
});

let clearField = (fields) => {
    fields.forEach((el, i) => {
        el.value = '';
    });
};

let getData = (elements) => {
    let data = {};
    elements.forEach((el, i) => {
        data[el.getAttribute('name')] = el.value;
    });
    return data;
};

let editData = () => {
    let tr = tbody.querySelectorAll('.user-list__tbody tr');
    tr.forEach((el, i) => {
        el.addEventListener('click', () => {
            el.remove();
            window.scrollTo(0, 0);
            let fields = el.querySelectorAll('input'),
                data = getData(fields);

            registerFiled.forEach((field, index) => {
                field.value = data[field.getAttribute('name')];
            });
        });
    });
};

let random = (min, max) => {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    rand = Math.round(rand);
    return rand;
};

let phoneMask = (el) => {
    el.addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });
};

phoneMask(phoneField);



winnerForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let tr = tbody.querySelectorAll('tr'),
        field = winnerForm.querySelector('input');
    if (tr.length == 0) {
        field.value = 'No registers users';
    } else {
        let trRandom = tr[random(1, tr.length) - 1],
            name = trRandom.querySelector('[name = "name"]').value,
            lastName = trRandom.querySelector('[name = "last_name"]').value,
            fullName = name + ' ' + lastName;
        field.value = fullName;
    }
});

